from glob import glob

def get_unit_temp():
    from os import system
    import glob
    import time
    base_dir = '/sys/bus/w1/devices/28*'
    devices = glob.glob(base_dir)
    temp_dict={}
    for i,device in enumerate(devices):
        with open(device+"/w1_slave",'r') as f:
            data = (f.readlines())[1].rstrip()
            temp = "%.1f" % (int(data[data.find("t=")+2:])/1000)
        temp_dict["T"+str(i+1)]=temp
    return temp_dict