from sys import argv
from pi_base import InitPins
import sqlite3
from time import sleep

WAIT_BEFORE_ENERGIZE = 2 #waiting for swithcing between cooling and heating
def connect_db(db_address):
    return sqlite3.connect(db_address)

def check_status():
    conn = connect_db(db_address)
    cur = conn.cursor()
    sql = "select custom_name from pins where board_name IN \
    ('Relay 2','Relay 3', 'Relay 4') order by board_name"
    cur.execute(sql)
    pins_names = tuple(map(lambda x: x[0],cur.fetchall()))
    conn.close()
    pins = InitPins.relay_status([2,3,4])
    pins_status = tuple(['On' if state else 'Off' for state in pins])
    print("-".join(["%s: %s" %(pin_name,pin_state) \
    for pin_name,pin_state in zip(pins_names,pins_status)]))

def find_mode():
    heating_pattern = (1,0,1)
    cooling_pattern = (0,1,1)
    gpios = InitPins()
    current_mode = gpios.relay_status([2,3,4])
    if(current_mode==heating_pattern):return 2
    elif(current_mode==cooling_pattern):return 1
    else:return 0


def heating():
    heating_pattern = (1,0,1)
    #to make sure Relay 3 is off
    gpios = InitPins()
    if(heating_pattern==gpios.relay_status([2,3,4])):
        print("It is already under test")
        del gpios
        return
    while(gpios.relay_status([3])[0]):
        gpios.denergize_relay(3)
    
    sleep(WAIT_BEFORE_ENERGIZE)
    #energizing Relay 2 which is for Heating
    gpios.energize_relay(2)
    #energizing Fan as well, which is Relay 4
    gpios.energize_relay(4)
    print("Fan and Heating unit has been turned on")
    del gpios

def cooling():
    cooling_pattern = (0,1,1)
    #to make sure Relay 2 is off
    gpios = InitPins()
    if(cooling_pattern==gpios.relay_status([2,3,4])):
        print("It is already under test")
        del gpios
        return
    while(gpios.relay_status([2])[0]):
        gpios.denergize_relay(2)
    
    sleep(WAIT_BEFORE_ENERGIZE)
    #energizing Relay 3 which is for Cooling
    gpios.energize_relay(3)
    #energizing Fan as well, which is Relay 4
    gpios.energize_relay(4)
    print("Fan and Cooling unit has been turned on")
    del gpios

def stop_testing():
        stop_pattern=(0,0,0)
        gpios = InitPins()
        if(stop_pattern==gpios.relay_status([2,3,4])):
            print("There is no test running")
            del gpios
            return
        gpios.denergize_relay([2,3,4])   
        print("Test has been stopped")

if __name__ == "__main__":
    db_address = "/home/pi/dataBase/radius.db"
    argc = len(argv)
    
    if(argc==1):
        print("controlling temp")
    
    elif(argc==2 and argv[1]=="S"):
        check_status()
    
    elif(argc==2 and argv[1]=="C"):
        cooling()
    
    elif(argc==2 and argv[1]=="H"):
        heating()


    elif(argc==2 and argv[1]=="ST"):
        stop_testing()
