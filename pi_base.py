__author__ = "Javad Bafekr"

import RPi.GPIO as IO
IO.setmode(IO.BCM)
IO.setwarnings(False)


class InitPins:
    def __init__(self):
        self.relays_dict =  {1:17,2:27,3:12,4:19}
        self.dout_dict = {1:21,2:20,3:25,4:26,5:24}
        self.din_dict = {1:23,2:18,3:5,4:4,5:6}
        # self.out_pins = (17,27,12,19,21,20,25,26,24)
        # self.in_pins = (23,18,5,4,6)
        ret = [IO.setup(pin,IO.IN) for pin in self.din_dict.values()]
        ret = [IO.setup(pin,IO.OUT) for pin in self.relays_dict.values()]
        ret = [IO.setup(pin,IO.OUT) for pin in self.dout_dict.values()]
        del ret

    def energize_relay(self,relay_number:int):
        (IO.output(self.relays_dict[relay_number],IO.HIGH))

    def denergize_relay(self,relay_number):
        if(isinstance(relay_number,int)):
            (IO.output(self.relays_dict[relay_number],IO.LOW))
        elif(isinstance(relay_number,list)):
            tuple(map(lambda rel_num: IO.output(self.relays_dict[rel_num],IO.LOW),
            relay_number))


    @classmethod
    def relay_status(cls,relay_numbers:[]):
        assert max(relay_numbers)<= 4, "Invalid Relay Number\nPlease use 1~4 for relay number" 
        __pins =cls()
        return tuple(map(lambda relay_number: IO.input(__pins.relays_dict[relay_number]),relay_numbers))

        
    @classmethod
    def do_status(cls,do_numbers:[]):
        assert max(do_numbers)<=5, "Invalid DO number\nPlease use 1~5 for DO Number\
        \te.g. InitPins.do_status([1,2,5])"
        __pins = cls()
        return tuple(map(lambda do_number: IO.input(__pins.dout_dict[do_number]),do_numbers))
    
    @classmethod
    def di_status(cls,di_numbers):
        assert max(di_numbers)<=5, "Invalid DI number\nPlease use 1~5 for DI Number\t\
        e.g. InitPins.di_status([1,2,5])"
        __pins = cls()
        return tuple(map(lambda di_number: IO.input(__pins.din_dict[di_number]),di_numbers))
