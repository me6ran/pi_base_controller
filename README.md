### pi_base_controller

The program will work on ElectronBits piece of hardware 
(https://www.electronbits.com/product/raspberry-pi-base)

A module for almost the whole capabilities of the hardware and other scripts for
controlling my specific design. you may feel free to use the whole codes for your
own projects in both **commercial** and **educational** field.

__author__ : Javad Bafekr :)
