from read_temperature import get_unit_temp
from cool_heat_testing import heating,cooling,find_mode,stop_testing



OFFSET = 3
low_temp_address = "/home/pi/shfiles/cool_heat/low_temp"
hi_temp_address = "/home/pi/shfiles/cool_heat/high_temp"
HEATING_MODE=2
COOLING_MODE=1
STOP_MODE=0
def get_low_temp():
    try:
        with open(low_temp_address,'r') as f:
            data = f.readline()
        return int(data)
    except:
        exit(1)

def get_high_temp():
    try:
        with open(hi_temp_address,'r') as f:
            data = f.readline()
        return int(data)
    except:
        exit(1)

if __name__ == "__main__":
    unit_temp = round(float(get_unit_temp()['T1']))
    low_temp = get_low_temp()
    high_temp = get_high_temp()
    ac_mode = find_mode()
    
    if(ac_mode==HEATING_MODE):
        #check the temp and make sure it is below than low + offset
        # print("heating mode")
        expected_temp = low_temp + OFFSET
        if(expected_temp<unit_temp):
            while not (find_mode()==STOP_MODE):
                stop_testing()
        
        
    elif(ac_mode==COOLING_MODE):
        #check the unit temp and make sure it is higher than high - offset
        #then go to stop mode
        # print("cooling mode")
        expected_temp = high_temp - OFFSET
        if(expected_temp>unit_temp):
            while not (find_mode()==STOP_MODE):
                stop_testing()
    
    elif(ac_mode==STOP_MODE):
        # print("stop mode")
        if(low_temp>unit_temp):heating()
        elif(high_temp<unit_temp):cooling()
        #check the temp to make descition to heat of cool
    

    
